#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Extensions;
using UnityEngine.Tilemaps;
using System.Collections.Generic;
using System.Collections;
using System;

namespace TwoMinutesToLive
{
	//[ExecuteInEditMode]
	public class World : EditorScript
	{
		public static World instance;
		public Transform trs;
		public Transform piecesParent;
	}

	[CustomEditor(typeof(World))]
	public class WorldEditor : EditorScriptEditor
	{
	}
}
#else
namespace TwoMinutesToLive
{
	public class World : EditorScript
	{
	}
}
#endif