using UnityEngine;
using Extensions;
using System.Collections.Generic;

namespace TwoMinutesToLive
{
	public class Trap : MonoBehaviour, IUpdatable, IResetable
	{
		public int damage;
		public bool PauseWhileUnfocused
		{
			get
			{
				return false;
			}
		}

		public virtual void DoUpdate ()
		{
		}

		public virtual void Reset ()
		{
		}
	}
}