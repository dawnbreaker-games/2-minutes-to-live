using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace TwoMinutesToLive
{
	public class Enemy : Entity, IResetable
	{
		public Renderer renderer;

		public virtual void Reset ()
		{
			hp = maxHp;
			gameObject.SetActive(true);
		}

		public override void Death ()
		{
			base.Death ();
			gameObject.SetActive(false);
			AudioClip deathSound = AudioManager.instance.deathSounds[Random.Range(0, AudioManager.instance.deathSounds.Length)];
			SoundEffect soundEffect = AudioManager.instance.PlaySoundEffect(AudioManager.instance.deathSoundEffectPrefab, new SoundEffect.Settings(deathSound));
			DeathSoundEffect deathSoundEffect = soundEffect as DeathSoundEffect;
			deathSoundEffect.killer = Player.instance;
		}
	}
}