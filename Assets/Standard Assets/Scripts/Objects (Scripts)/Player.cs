using Extensions;
using UnityEngine;
using UnityEngine.UI;
// using UnityEngine.InputSystem;
// using UnityEngine.InputSystem.Controls;
// using TouchPhase = UnityEngine.InputSystem.TouchPhase;

namespace TwoMinutesToLive
{
	public class Player : Entity, ISaveableAndLoadable
	{
		public static Player instance;
		public delegate void OnMoved();
		public event OnMoved onMoved;
		public Transform hpIconParent;
		public LayerMask whatIsScroll;
		public LayerMask whatIsSavePoint;
		public LayerMask whatIsTrap;
		public LayerMask whatIsEnemy;
		public int uniqueId;
		[SaveAndLoadValue(false)]
		public Vector2 SpawnPosition
		{
			get
			{
				return trs.position;
			}
			set
			{
				trs.position = value;
			}
		}
		public int UniqueId
		{
			get
			{
				return uniqueId;
			}
			set
			{
				uniqueId = value;
			}
		}
		public RectTransform cameraCanvasRectTrs;
		public bool runOnMovedEvent = true;
		Vector2[] possibleMoves = new Vector2[0];
		bool inSafeZone;
		int moveInput;
		int previousMoveInput;
		Transform hpIcon;
		
		public override void OnEnable ()
		{
			instance = this;
			base.OnEnable ();
			hp = maxHp;
			hpIcon = hpIconParent.GetChild(0);
			int hpIconCount = hpIconParent.childCount;
			for (int i = hpIconCount; i < hp; i ++)
				Instantiate(hpIcon, hpIconParent);
			moveTimer.Reset ();
			possibleMoves = GameManager.instance.possibleMoves.Add(Vector2.zero);
		}

		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			moveInput = InputManager.MoveInput;
			HandleMoving ();
			previousMoveInput = moveInput;
		}

		public override void HandleMoving ()
		{
			if (moveInput > previousMoveInput)
			{
#if UNITY_EDITOR || UNITY_STANDALONE
				if (InputManager.LeftClickInput || InputManager.RightClickInput)
				{
					for (int i = 0; i < moveInput - previousMoveInput; i ++)
					{
						Vector2 desiredMove = GameCamera.instance.camera.ScreenToWorldPoint(InputManager.MousePosition) - trs.position;
						Vector2 move = desiredMove.GetClosestPoint(possibleMoves);
						if (Physics2D.OverlapPoint((Vector2) trs.position + move, whatICantMoveTo) == null)
							Move (move);
						else
							Move (Vector2.zero);
					}
				}
#endif
				for (int i = 0; i < Input.touchCount; i ++)
				{
					Touch touch = Input.touches[i];
					if (touch.phase == UnityEngine.TouchPhase.Began)
					{
						Vector2 desiredMove = GameCamera.instance.camera.ScreenToWorldPoint(touch.position) - trs.position;
						Vector2 move = desiredMove.GetClosestPoint(possibleMoves);
						if (Physics2D.OverlapPoint((Vector2) trs.position + move, whatICantMoveTo) == null)
							Move (move);
						else
							Move (Vector2.zero);
					}
				}
			}
			// foreach (TouchControl touch in Touchscreen.current.touches)
			// {
			// 	if (touch.phase.ReadValue() == TouchPhase.Began)
			// 	{
			// 		Vector2 desiredMove = GameCamera.instance.camera.ScreenToWorldPoint(touch.position.ToVec2()) - trs.position;
			// 		int indexOfClosestPossibleMove = desiredMove.GetIndexOfClosestPoint(possibleMoves);
			// 		Vector2 move = possibleMoves[indexOfClosestPossibleMove];
			// 		if (Physics2D.OverlapPoint((Vector2) trs.position + move, whatICantMoveTo) == null)
			// 			Move (move);
			// 	}
			// }
		}

		public override bool Move (Vector2 move)
		{
			moveIsReady = true;
			if (base.Move(move))
			{
				OnMove ();
				return true;
			}
			else
				OnMove ();
			return false;
		}

		public void OnMove ()
		{
			if (runOnMovedEvent && onMoved != null)
				onMoved ();
			// inSafeZone = CheckForSafeZone ();
			// if (inSafeZone)
			// 	CheckForSavePoint ();
			// else
			// {
			// 	CheckForDangerZone ();
			// 	CheckForBullet ();
			// }
			CheckForEnemies ();
			CheckForTrap ();
			CheckForScroll ();
			// HandeSafeZoneIcons ();
			GameCamera.instance.camera.Render();
			Canvas.ForceUpdateCanvases();
		}

		// void HandeSafeZoneIcons ()
		// {
		// 	foreach (IconForSafeZone iconForSafeZone in IconForSafeZone.instances)
		// 	{
		// 		if (inSafeZone)
		// 			iconForSafeZone.rectTrs.localPosition = cameraCanvasRectTrs.sizeDelta.Multiply(GameCamera.instance.camera.WorldToViewportPoint(iconForSafeZone.trs.position)) - cameraCanvasRectTrs.sizeDelta / 2;
		// 		iconForSafeZone.image.enabled = inSafeZone;
		// 	}
		// }

		bool CheckForScroll ()
		{
			Collider2D hitCollider = Physics2D.OverlapPoint(trs.position, whatIsScroll);
			if (hitCollider != null)
			{
				Scroll currentlyReading = hitCollider.GetComponent<Scroll>();
				GameManager.instance.textPanelText.text.text = currentlyReading.text;
				GameManager.instance.textPanelGo.SetActive(true);
				return true;
			}
			else if (GameManager.instance.textPanelGo != null)
				GameManager.instance.textPanelGo.SetActive(false);
			return false;
		}

		// bool CheckForSavePoint ()
		// {
		// 	Collider2D hitCollider = Physics2D.OverlapPoint(trs.position, whatIsSavePoint);
		// 	if (hitCollider != null)
		// 	{
		// 		FullHeal ();
		// 		SpawnPosition = trs.position;
		// 		SaveAndLoadManager.instance.Save ();
		// 		return true;
		// 	}
		// 	return false;
		// }

		bool CheckForTrap ()
		{
			Collider2D hitCollider = Physics2D.OverlapPoint(trs.position, whatIsTrap);
			if (hitCollider != null)
			{
				Trap trap = hitCollider.GetComponent<Trap>();
				TakeDamage (trap.damage);
				return true;
			}
			return false;
		}

		bool CheckForEnemies ()
		{
			Collider2D[] hitColliders = Physics2D.OverlapPointAll(trs.position, whatIsEnemy);
			Enemy[] enemies = new Enemy[hitColliders.Length];
			for (int i = 0; i < hitColliders.Length; i ++)
			{
				Collider2D hitCollider = hitColliders[i];
				enemies[i] = hitCollider.GetComponent<Enemy>();
			}
			if (enemies.Length > 0)
			{
				StartBattle (enemies);
				return true;
			}
			return false;
		}

		void StartBattle (Enemy[] enemies)
		{
			Bounds[] enemiesBounds = new Bounds[enemies.Length];
			for (int i = 0; i < enemies.Length; i ++)
			{
				Enemy enemy = enemies[i];
				enemiesBounds[i] = enemy.renderer.bounds;
			}
			GameCamera.instance.viewSize = BoundsExtensions.Combine(enemiesBounds).size;
			GameCamera.instance.HandleViewSize ();
			GameCamera.instance.camera.Render();
		}

		public override void TakeDamage (float amount)
		{
			if (isDead)
				return;
			if ((int) (hp - amount) < (int) hp)
			// for (int i = (int) (hp - amount); i < (int) hp; i ++)
				Destroy(hpIconParent.GetChild(0).gameObject);
			base.TakeDamage (amount);
		}

		public void FullHeal ()
		{
			if (hp <= 0)
			{
				Death ();
				return;
			}
			Transform hpIconTrs = hpIconParent.GetChild(0);
			for (int i = (int) hp; i < maxHp; i ++)
				Instantiate(hpIconTrs, hpIconParent);
			hp = maxHp;
		}

		public override void Death ()
		{
			base.Death ();
			GameManager.paused = true;
			AudioClip deathSound = AudioManager.instance.deathSounds[Random.Range(0, AudioManager.instance.deathSounds.Length)];
			SoundEffect soundEffect = AudioManager.instance.PlaySoundEffect (AudioManager.instance.deathSoundEffectPrefab, new SoundEffect.Settings(deathSound));
			DeathSoundEffect deathSoundEffect = soundEffect as DeathSoundEffect;
			deathSoundEffect.killer = FindObjectOfType<Enemy>();
			// for (int i = 0; i < EventManager.events.Count; i ++)
			// 	EventManager.events[i].time = Mathf.Infinity;
			EventManager.events.Clear();
			SaveAndLoadManager.lastUniqueId = SaveAndLoadManager.INIT_LAST_UNIQUE_ID;
			GameOverScreen.instance.Open ();
		}
	}
}