﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TwoMinutesToLive
{
	public class Scroll : MonoBehaviour
	{
		public Transform trs;
		[Multiline(15)]
		public string text;
	}
}