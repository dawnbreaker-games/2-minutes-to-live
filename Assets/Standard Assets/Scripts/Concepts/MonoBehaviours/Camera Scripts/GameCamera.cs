using Extensions;

namespace TwoMinutesToLive
{
	public class GameCamera : CameraScript
	{
		public AnimatedBackground animatedBackground;

		public override void HandleViewSize ()
		{
			base.HandleViewSize ();
			animatedBackground.rawImage.uvRect = viewRect.MultiplySize(animatedBackground.scale);
		}
	}
}