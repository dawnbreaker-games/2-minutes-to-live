using UnityEngine;
using UnityEngine.SceneManagement;

namespace TwoMinutesToLive
{
	public class PauseMenu : SingletonMonoBehaviour<PauseMenu>
	{
		public GameObject[] sceneButtonsGos = new GameObject[0];

		public override void Awake ()
		{
			base.Awake ();
			// GameManager.singletons.Remove(GetType());
			// GameManager.singletons.Add(GetType(), this);
			gameObject.SetActive(false);
		}

		public void Open ()
		{
			if (PauseMenu.instance != this)
			{
				PauseMenu.instance.Open ();
				return;
			}
			GameManager.instance.PauseGame (true);
			foreach (GameObject sceneButtonsGo in sceneButtonsGos)
			{
				if (sceneButtonsGo.name.Contains(SceneManager.GetActiveScene().name))
				{
					sceneButtonsGo.SetActive(false);
					break;
				}
			}
			gameObject.SetActive(true);
		}

		public void Close ()
		{
			if (PauseMenu.instance != this)
			{
				PauseMenu.instance.Close ();
				return;
			}
			gameObject.SetActive(false);
			GameManager.instance.PauseGame (false);
		}
	}
}