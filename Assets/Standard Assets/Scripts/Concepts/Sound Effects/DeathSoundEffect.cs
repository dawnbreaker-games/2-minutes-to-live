using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace TwoMinutesToLive
{
	public class DeathSoundEffect : SoundEffect
	{
		public Entity killer;

		public override void OnDisable ()
		{
			base.OnDisable ();
			if (killer == null || GameManager.paused)
				return;
			AudioClip deathResponse = AudioManager.instance.deathResponses[Random.Range(0, AudioManager.instance.deathResponses.Length)];
			if (killer.deathResponse == null || !killer.deathResponse.gameObject.activeInHierarchy)
				killer.deathResponse = AudioManager.instance.PlaySoundEffect (AudioManager.instance.deathSoundEffectPrefab, new SoundEffect.Settings(deathResponse));
		}
	}
}